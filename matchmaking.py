player_graph = {0: {1:0, 5:0},
                1: {3:5},
                2: {4:1},
                3: {},
                4: {5:2},
                5: {}}

skillof = [200,300,210,100,50,240]
sum_of_skills = sum(skillof)

val_weights = {}

import math
from weighted_selection import weighted_pick

test_list = []

for cases in range(10000):
    for node,W in player_graph.items():
        for neighbour in W:
            #TODO: Normalization of skill and number_of_matches is required.
            val_weights[(node,neighbour)] = math.exp((-0.5*abs((skillof[node] - skillof[neighbour])) - W[neighbour]))

    #predicted_edge
    pe = weighted_pick(val_weights)

    # Updating Number if matches
    player_graph[pe[0]][pe[1]] += 1
    test_list.append(pe)

    # Updating Skills
        


test_dict = {}

for e in test_list:
    try:
        test_dict[e] += 1
    except KeyError:
        test_dict[e] = 1

from pprint import pprint

pprint(test_dict)
